Develop a sample app for Android to test BLE Data transfer.

Requirements

- User should be able to scan and connect to a specified BLE device.
- User should be able to copy and paste token in the app interface.
- User should be able to write/pass a token to the connected BLE device.
- User should be able to see response from the connected BLE device.

Prerequisites
- Name of the BLE device to connect.
- UUIDs for characteristics to read/write data.

Reference
- https://github.com/kai-morich/SimpleBluetoothLeTerminal
