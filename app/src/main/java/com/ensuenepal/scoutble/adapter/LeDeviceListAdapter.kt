package com.ensuenepal.scoutble.adapter

import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ensuenepal.scoutble.R
import com.ensuenepal.scoutble.databinding.SingleLeDeviceBinding

// Adapter for holding devices found through scanning.

class LeDeviceListAdapter(
        private val mLeDevices: List<BluetoothDevice>,
        listener: LeDeviceClickListener
): RecyclerView.Adapter<LeDeviceListAdapter.LeDeviceViewHolder>() {

    private var leDeviceClickListener: LeDeviceClickListener = listener

    interface LeDeviceClickListener {
        fun onLeDeviceClick(leDevice: BluetoothDevice)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeDeviceViewHolder {
        val binding = SingleLeDeviceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LeDeviceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LeDeviceViewHolder, position: Int) {
        val leDevice: BluetoothDevice = mLeDevices[position]

        holder.itemView.setOnClickListener {
            leDeviceClickListener.onLeDeviceClick(leDevice)
        }

        holder.bindLeDevice(leDevice)
    }

    override fun getItemCount() = mLeDevices.size

    inner class LeDeviceViewHolder(private val binding: SingleLeDeviceBinding) : RecyclerView.ViewHolder(binding.root) {

        private var tvLeDeviceInfo: TextView = itemView.findViewById(R.id.tvLeDeviceInfo)

        fun bindLeDevice(leDevice: BluetoothDevice) {

            val deviceName = if (leDevice.name != null) {
                leDevice.name.toString()
            } else {
                "Unnamed"
            }
            tvLeDeviceInfo.setText("$deviceName\n${leDevice.address}")
        }
    }
}