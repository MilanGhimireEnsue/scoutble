package com.ensuenepal.scoutble.server

import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.content.Intent
import android.os.ParcelUuid
import android.util.Log
import com.ensuenepal.scoutble.client.BluetoothStatus
import com.ensuenepal.scoutble.utils.Constants

class BluetoothLeServerService(
    private val mBluetoothManager: BluetoothManager,
    private val mBluetoothLeServerResponseInterface: BluetoothLeServerResponseInterface
) : BluetoothLeServerCommandInterface {

    private var mGattServer: BluetoothGattServer? = null
    private var mBluetoothAdapter: BluetoothAdapter = mBluetoothManager.adapter
    private var mBluetoothLeAdvertiser: BluetoothLeAdvertiser

    init {
        mBluetoothLeAdvertiser = mBluetoothAdapter.bluetoothLeAdvertiser
    }

    /*
    * this variable is used to get the Bluetooth Gatt Server Callback which gives the
    * if some device requests to connect
    * and if that connected device request to write. In another words, client send us the data
    * */
    private val gattServerCallback = object : BluetoothGattServerCallback() {
        override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
            super.onConnectionStateChange(device, status, newState)

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d("onConnectionStateChange", "Connected by client: ${device?.address} with status: $status and newState: $newState")
                mBluetoothLeServerResponseInterface.onResponse(BluetoothStatus.DEVICE_CONNECTED,"Connected with client: ${device?.address} with status: $status and newState: $newState\n")
            }
            else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d("onConnectionStateChange", "Disconnected by client: ${device?.address} with status: $status and newState: $newState")
                mBluetoothLeServerResponseInterface.onResponse(BluetoothStatus.DEVICE_DISCONNECTED,"Disconnected by client: ${device?.address} with status: $status and newState: $newState\n")
            }
        }

        override fun onCharacteristicWriteRequest(device: BluetoothDevice?, requestId: Int, characteristic: BluetoothGattCharacteristic?, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray?) {
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value)
            mBluetoothLeServerResponseInterface.onResponse(BluetoothStatus.MESSAGE_RECEIVED,"Received: ${value?.toString(Charsets.UTF_8)}\n")

            Log.d("onCharacteristicWriteRe", "Client request to write: ${device?.address}")

            if (mGattServer == null) return

            if (characteristic != null) {
                if (characteristic.uuid != null) {
                    if (characteristic.uuid == Constants.CHARACTERISTIC_ECHO_UUID) {
                        mGattServer?.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null)
                        characteristic.value = "ab".toByteArray()
                        mBluetoothLeServerResponseInterface.onResponse(BluetoothStatus.MESSAGE_SEND,"Sending: ab")
                        mGattServer?.notifyCharacteristicChanged(device, characteristic, false)
                    }
                }

            }
        }
    }

    /*
    * this callback variable implements abstract class AdvertiseCallback which helps to know
    * if the advertising is successful or not
    * */
    private val mAdvertiseCallback = object : AdvertiseCallback() {
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            mBluetoothLeServerResponseInterface.onResponse(BluetoothStatus.ADVERTISING_STARTED,"Peripheral advertising started. $settingsInEffect\n")
            Log.d("Advertise", "Peripheral advertising started.");
//            super.onStartSuccess(settingsInEffect)
        }

        override fun onStartFailure(errorCode: Int) {
            mBluetoothLeServerResponseInterface.onResponse(BluetoothStatus.ADVERTISING_ERROR,"Peripheral advertising failed: $errorCode")
            Log.d("Advertise", "Peripheral advertising failed: $errorCode");
//            super.onStartFailure(errorCode)
        }
    }

    /* start implementation of BluetoothLe Server Command Interface */

    override fun setupServer(context: Context) {
        mGattServer = mBluetoothManager.openGattServer(context, gattServerCallback)

        // setup Server
        val serverUuid = Constants.SERVICE_UUID
        val service = BluetoothGattService(serverUuid, BluetoothGattService.SERVICE_TYPE_PRIMARY)

        // write characteristic
        val writeCharacteristic = BluetoothGattCharacteristic(Constants.CHARACTERISTIC_ECHO_UUID, BluetoothGattCharacteristic.PROPERTY_WRITE, BluetoothGattCharacteristic.PERMISSION_WRITE)

        service.addCharacteristic(writeCharacteristic)
        mGattServer?.addService(service)

        // end setup Server
    }

    override fun startAdvertising() {
        if (!mBluetoothAdapter.isEnabled) {
            Log.d("Advertise", "mBluetoothAdapter isEnabled")
            return
        }

        if (!mBluetoothAdapter.isMultipleAdvertisementSupported) {
            Log.d("Advertise", "mBluetoothAdapter isMultipleAdvertisementSupported")
            return
        }

        // start advertising
        val settings = AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setConnectable(true)
                .setTimeout(0)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW)
                .build()

        val parcelUuid = ParcelUuid.fromString(Constants.SERVICE_STRING)
        val data = AdvertiseData.Builder()
                .setIncludeDeviceName(false)
                .addServiceUuid(parcelUuid)
                .build()

        Log.d("Advertise", "Callback before")
        mBluetoothLeAdvertiser.startAdvertising(settings, data, mAdvertiseCallback)
    }
}