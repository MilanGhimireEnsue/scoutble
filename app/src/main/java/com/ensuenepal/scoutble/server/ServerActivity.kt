package com.ensuenepal.scoutble.server

import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.ParcelUuid
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.ensuenepal.scoutble.client.BluetoothStatus
import com.ensuenepal.scoutble.databinding.ActivityServerBinding
import com.ensuenepal.scoutble.utils.Constants.Companion.SERVICE_STRING
import java.util.*

/*
* https://source.android.com/devices/bluetooth/ble_advertising
*https://github.com/tutsplus/Android-BluetoohLEAdvertising
* https://code.tutsplus.com/tutorials/how-to-advertise-android-as-a-bluetooth-le-peripheral--cms-25426
* https://www.bignerdranch.com/blog/bluetooth-low-energy-on-android-part-1/
* https://developer.android.com/reference/android/bluetooth/BluetoothGattServer.html#summary
* https://stackoverflow.com/questions/29235787/how-can-i-create-an-android-ble-peripheral
* https://industrialsmart.atlassian.net/browse/SSD-122
*
* */
/*
* https://github.com/tutsplus/Android-BluetoohLEAdvertising
* */

class ServerActivity : AppCompatActivity(), BluetoothLeServerResponseInterface {
    private lateinit var gattActivityBinding: ActivityServerBinding

    private lateinit var mBluetoothManager: BluetoothManager

    private lateinit var mBluetoothLeServerService: BluetoothLeServerCommandInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        gattActivityBinding = ActivityServerBinding.inflate(layoutInflater)
        val view = gattActivityBinding.root
        setContentView(view)

        mBluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager

        mBluetoothLeServerService = BluetoothLeServerService(mBluetoothManager, this@ServerActivity)

        setupServer()

    }

    private fun setupServer() {
        mBluetoothLeServerService.setupServer(this@ServerActivity)
    }

    private fun checkServiceAvailability() {
        Log.d("Advertise", "Started.")
        val mBluetoothManager = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        val mBluetoothAdapter = mBluetoothManager.adapter

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled) {
            Log.d("Advertise", "mBluetoothAdapter isEnabled")
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivity(enableBtIntent)
            finish()
            return
        }
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Log.d("Advertise", "hasSystemFeature FEATURE_BLUETOOTH_LE")
            finish();
            return;
        }

        if (!mBluetoothAdapter.isMultipleAdvertisementSupported) {
            Log.d("Advertise", "mBluetoothAdapter isMultipleAdvertisementSupported")
            finish()
            return
        }

        mBluetoothLeServerService.startAdvertising()
    }

    override fun onResume() {
        super.onResume()
        checkServiceAvailability()
    }

    /* Implementation of BluetoothLe Server Response Interface */

    /*
    * This method writes to provided message to text view.
    * */
    override fun onResponse(status: BluetoothStatus, message: String) {
        runOnUiThread(Runnable {
            gattActivityBinding.tvGattServerInfo.append("$message\n")
        })
    }
}