package com.ensuenepal.scoutble.server

import android.content.Context
import com.ensuenepal.scoutble.client.BluetoothStatus

interface BluetoothLeServerCommandInterface {

    /**
    * This method is used to setup gattServerCallback, add services and add characteristics
    * */
    fun setupServer(context: Context)

    /**
    * This method is used to start Advertisements (Broadcasting, Discovery, Advertisement parameters, Advertisement data)
    * */
    fun startAdvertising()

}

interface BluetoothLeServerResponseInterface {

    /*
    * Sends service messages to the caller
    * */
    fun onResponse(status: BluetoothStatus, message: String)

}