package com.ensuenepal.scoutble.client

import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import com.ensuenepal.scoutble.utils.Constants
import com.ensuenepal.scoutble.utils.Constants.Companion.SERVICE_UUID
import java.lang.Exception

class BluetoothLeClientService(
        private var bluetoothLeScanner: BluetoothLeScanner,
        private val mBluetoothLeClientResponseInterface: BluetoothLeClientResponseInterface
): BluetoothLeClientCommandInterface {

    private var discoveredDevices = mutableListOf<BluetoothDevice>()
    private var mConnected: Boolean = false
    private var mGatt: BluetoothGatt? = null
    private var mScanning: Boolean = false


    /**
    * The callback handles connection status with BLE Peripheral and responds status back to the listener.
    *  Moreover, it initiates discovery of services if connection is established with BLE peripheral.
    * */
    private val mBluetoothGattCallback = object : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            val deviceAddress = gatt?.device?.address

            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    try {
                        mBluetoothLeClientResponseInterface.onResponse(BluetoothStatus.DEVICE_CONNECTED, "Successfully connected to $deviceAddress")
                    } catch (ex: Exception) {
                        Log.e(TAG, "Exception occurred: ${ex.message}")
                    }
                    Log.d(TAG, "Successfully connected to $deviceAddress")

                    mConnected = true
                    gatt?.discoverServices()
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    Log.d(TAG, "Successfully disconnected from $deviceAddress")
                    mBluetoothLeClientResponseInterface.onResponse(BluetoothStatus.DEVICE_DISCONNECTED, "Successfully disconnected from $deviceAddress")
                    gatt?.close()
                }
            } else {

                /*
                * error 133 happen most frequently in one of the two following scenarios:
                * The BluetoothDevice we’re trying to connect to is no longer advertising or within Bluetooth range, and the connectGatt() call has timed out after about 30 seconds of trying to connect to it with the autoConnect argument set to false.
                * The firmware running on the BLE device has rejected the connection attempt by Android.
                */
                Log.d(TAG, "Error $status encountered for $deviceAddress! Disconnecting...")
                mBluetoothLeClientResponseInterface.onResponse(BluetoothStatus.ERROR_OCCURRED, "Error $status encountered for $deviceAddress! Disconnecting...")

                gatt?.close()
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            Log.d(TAG, "onServicesDiscovered Result is ${gatt.toString()} with status: $status")
            if (status != BluetoothGatt.GATT_SUCCESS) {
                return
            }

            Log.i(TAG, "onServicesDiscovered Discovered ${gatt?.services?.size} services for ${gatt?.device?.address}")
            // Consider connection setup as complete here
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)
            Log.d(TAG, "onCharacteristicWrite Client Request onCharacteristicChanged ${gatt.toString()} ${characteristic?.value.contentToString()}")
            val receivedMessage = "Received: ${characteristic?.value?.toString(Charsets.UTF_8)}"
            mBluetoothLeClientResponseInterface.onResponse(BluetoothStatus.MESSAGE_RECEIVED, receivedMessage)
        }
    }

    /*
    * This method adds the discovered device to the discoveredDevices variable where all the unique discovered devices are kept
    * */
    private fun addDiscoveredDevicesToList(scanResult: ScanResult) {
        if (discoveredDevices.indexOf(scanResult.device) < 0) {
            discoveredDevices.add(scanResult.device)
            mBluetoothLeClientResponseInterface.onScanBleResult(scanResult.device)
        }
    }

    /*
    * ScanCallback is used to get the devices discovered by the BLE
    * */
    private val leScanCallback: ScanCallback = object : ScanCallback() {

        override fun onScanResult(callbackType: Int, result: ScanResult) {
            Log.d("onScanResult", "Data is: CallbackType- $callbackType and result - $result")
            Log.d("onScanResult", "Device Info: ${result.device}")
            addDiscoveredDevicesToList(result)
        }
    }

    /* Implementation of Bluetooth Client Command Interface */
    override fun scan() {
        mScanning = true
        discoveredDevices.clear()

        Log.d("scanLeDevices", "scan started")
        val scanFilter = ScanFilter.Builder()
                .setServiceUuid(ParcelUuid(SERVICE_UUID))
                .build()

        // Note: Filtering does not work the same (or at all) on most devices. It also is unable to
        // search for a mask or anything less than a full UUID.
        // Unless the full UUID of the server is known, manual filtering may be necessary.
        // For example, when looking for a brand of device that contains a char sequence in the UUID
        // in simple words: Its for searching advertise that we are interested in
        val filters = arrayListOf(scanFilter)

        val scanSettings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build()
        this.bluetoothLeScanner.startScan( leScanCallback)
        mBluetoothLeClientResponseInterface.onResponse(BluetoothStatus.SCAN_STARTED,"Scanning started.\n")
    }

    override fun stopBleScan() {
        if (mScanning) {
            this.bluetoothLeScanner.stopScan(leScanCallback)
            mScanning = false
            mBluetoothLeClientResponseInterface.onResponse(BluetoothStatus.SCAN_STOPPED,"Stopped scanning.\n")
        }
    }

    override fun connectBle(context: Context, device: BluetoothDevice) {
        mGatt = device.connectGatt(context, false, mBluetoothGattCallback)
    }

    override fun write(message: String) {
        Log.d("sendMessage", "sending message from here.")
        if (!mConnected) return

        val service = mGatt?.getService(SERVICE_UUID)
        val characteristic = service?.getCharacteristic(Constants.CHARACTERISTIC_ECHO_UUID)
        val messageBytes = message.toByteArray()
        characteristic?.value = messageBytes
        val success = mGatt?.writeCharacteristic(characteristic)
        Log.d("Success", "Value is ${success.toString()}")
    }

    companion object {
        const val TAG = "BLeClientService"
    }

}


