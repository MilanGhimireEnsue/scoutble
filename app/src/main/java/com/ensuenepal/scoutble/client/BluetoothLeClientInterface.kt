package com.ensuenepal.scoutble.client

import android.bluetooth.BluetoothDevice
import android.content.Context

enum class BluetoothStatus {
    SCAN_STARTED,
    SCAN_STOPPED,
    DEVICE_DISCONNECTED,
    DEVICE_CONNECTED,
    ERROR_OCCURRED,
    MESSAGE_SEND,
    MESSAGE_RECEIVED,
    ADVERTISING_STARTED,
    ADVERTISING_STOPPED,
    ADVERTISING_ERROR,
}

interface BluetoothLeClientCommandInterface {

    /*
    * The method scans for BLE devices
    * */
    fun scan()

    /*
    * This method stops the scan of BLE device that was starts by scanBle() method
    * */
    fun stopBleScan()

    /*
    * This method is used to request for connection with the BLE device
    * */
    fun connectBle(context: Context, device: BluetoothDevice)

    /*
    * This method is used to send message to the connected device/Server
    * */
    fun write(message: String)
}

interface BluetoothLeClientResponseInterface {

    /*
    * This method send the discovered devices
    * */
    fun onScanBleResult(device: BluetoothDevice)

    /*
    * The method sends service messages to the caller.
    * */
    fun onResponse(status: BluetoothStatus, message: String)
}