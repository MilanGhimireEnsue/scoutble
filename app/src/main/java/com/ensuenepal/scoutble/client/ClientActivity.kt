package com.ensuenepal.scoutble.client

import android.Manifest
import android.app.Activity
import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.ensuenepal.scoutble.R
import com.ensuenepal.scoutble.server.ServerActivity
import com.ensuenepal.scoutble.adapter.LeDeviceListAdapter
import com.ensuenepal.scoutble.databinding.ActivityClientBinding
import java.util.*

/*
* References for BLE
* https://developer.android.com/guide/topics/connectivity/bluetooth-le
* https://stackoverflow.com/questions/59544729/how-i-can-scanning-ble-in-fragment-on-android-studio
* https://android.googlesource.com/platform/development/+/7167a054a8027f75025c865322fa84791a9b3bd1/samples/BluetoothLeGatt/src/com/example/bluetooth/le/DeviceScanActivity.java
* https://punchthrough.com/android-ble-guide/
*
* For Peripheral BLE
* https://www.bignerdranch.com/blog/bluetooth-low-energy-on-android-part-1/
* https://code.tutsplus.com/tutorials/how-to-advertise-android-as-a-bluetooth-le-peripheral--cms-25426
*
* */
class ClientActivity : AppCompatActivity(), BluetoothLeClientResponseInterface {

    private lateinit var mainActivityBinding: ActivityClientBinding
    private lateinit var mBluetoothAdapter: BluetoothAdapter

    private fun PackageManager.missingSystemFeature(name: String): Boolean = !hasSystemFeature(name)
    private val REQUEST_ENABLE_BT = 1
    private var isScanning = false
    // Stops scanning after 10 seconds.
    private val SCAN_PERIOD: Long = 10000

    private val discoveredDevices = mutableListOf<BluetoothDevice>()
    private lateinit var leDeviceAdapter: LeDeviceListAdapter

    private var bluetoothLeScanner: BluetoothLeScanner? = null

    // for reusable component started.
    lateinit var bluetoothLeClientService: BluetoothLeClientCommandInterface

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivityBinding = ActivityClientBinding.inflate(layoutInflater)
        val view = mainActivityBinding.root
        setContentView(view)
    }

    override fun onResume() {
        super.onResume()

        checkForSupport()
        checkIfBluetoothStatus()
        requestPermissions()
        initClickListener()
        initRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.gatt_server, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.menu_server -> {
                val intent = Intent(this@ClientActivity, ServerActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /*
    * This method is used for loading list of devices in UI using RecyclerView
    * */
    private fun initRecyclerView() {
        mainActivityBinding.rvLeDevice.layoutManager = LinearLayoutManager(this@ClientActivity)

        leDeviceAdapter = LeDeviceListAdapter(
                discoveredDevices,
                object : LeDeviceListAdapter.LeDeviceClickListener {
                    override fun onLeDeviceClick(leDevice: BluetoothDevice) {
                        Log.d("MainActivity", "LeDevice clicked. ${leDevice.address}")

//                        NOTE: you should always stop your BLE scan before connecting to a BLE device
                        if (isScanning) {
                            bluetoothLeClientService.stopBleScan()
                        }

                        bluetoothLeClientService.connectBle(this@ClientActivity, leDevice)

                    }
                }
        )
        mainActivityBinding.rvLeDevice.adapter = leDeviceAdapter
        leDeviceAdapter.notifyDataSetChanged()
    }

    private fun requestPermissions() {
        val permissionToRequest = mutableListOf<String>()
        if (!hasCoarseLocationPermission()) {
            permissionToRequest.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }

        if (permissionToRequest.isNotEmpty()) {
            ActivityCompat.requestPermissions(this@ClientActivity, permissionToRequest.toTypedArray(), 0)
        }
    }

    private fun hasCoarseLocationPermission() =
            ActivityCompat.checkSelfPermission(
                    this@ClientActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 0 && grantResults.isNotEmpty()) {
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("MainActivity", "Permission granted ${permissions[i]}")
                }
            }
        }
    }

    /*
    * This method is used to setup the click events in Client Activity
    * */
    private fun initClickListener() {
        mainActivityBinding.btnScan.setOnClickListener {
            checkIfBluetoothStatus()
            if (!isScanning) {
                scanLeDevice()
            }
        }

        mainActivityBinding.btnSendMessage.setOnClickListener {
            bluetoothLeClientService.write(mainActivityBinding.etMessage.text.toString())
        }
    }

    /*
    * This method starts scanning for devices
    * */
    private fun scanLeDevice() {
        discoveredDevices.clear()
        leDeviceAdapter.notifyDataSetChanged()
        mainActivityBinding.tvResult.text = ""
        if (!isScanning) { // Stops scanning after a pre-defined scan period.
            Log.d("scanLeDevices", "scan started")
            mainActivityBinding.tvResult.append("Started scanning for devices.\n")
            isScanning = true

            Handler(Looper.getMainLooper()).postDelayed({
                bluetoothLeClientService.stopBleScan()
            }, SCAN_PERIOD)
            bluetoothLeClientService.scan()
        } else {
            isScanning = false
            bluetoothLeClientService.stopBleScan()
        }
    }

    private fun checkIfBluetoothStatus() {
        // Initializes Bluetooth adapter.
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        bluetoothLeScanner = mBluetoothAdapter.bluetoothLeScanner
        bluetoothLeScanner?.let {
            this.bluetoothLeClientService =
                    BluetoothLeClientService(it, this@ClientActivity)
        }

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(this, R.string.thank_you_for_enabling, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, R.string.request_rejected, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun checkForSupport() {
//        makeAvailable to BLE un supporting devices
        packageManager.takeIf { it.missingSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE) }?.also {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    /* Implementation of BluetoothLe Client Response Interface*/

    /*
    * This method writes the given response to the text view
    * */
    override fun onResponse(status: BluetoothStatus, message: String) {
        runOnUiThread(Runnable {
            mainActivityBinding.tvResult.append("$message\n")
        })
    }

    /*
    * This method adds the discovered devices to list
    * */
    override fun onScanBleResult(device: BluetoothDevice) {
        mainActivityBinding.tvResult.append("$device device found.\n")
        discoveredDevices.add(device)
        leDeviceAdapter.notifyDataSetChanged()
    }
}