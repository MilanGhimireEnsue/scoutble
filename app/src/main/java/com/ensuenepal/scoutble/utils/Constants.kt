package com.ensuenepal.scoutble.utils

import java.util.*

class Constants {

    companion object {
        const val SERVICE_STRING = "7D2EA28A-F7BD-485A-BD9D-92AD6ECFE93E"
        val SERVICE_UUID = UUID.fromString(SERVICE_STRING)

        const val CHARACTERISTIC_ECHO_STRING = "7D2EBAAD-F7BD-485A-BD9D-92AD6ECFE93E"
        val CHARACTERISTIC_ECHO_UUID = UUID.fromString(CHARACTERISTIC_ECHO_STRING)
    }
}